// Variável de cache que vai armazenar o número da esquerda nas operações
var cache1 = null;
// Flag que indicará se um dos operadores foi pressionado ou não (usada para limpar o visor depois que um operador é pressionado)
var pressionouOperador = false;
// Variável de cache que vai armazenar o operador selecionado
var cacheOperador = null;
// Variável que armazenará o input do visor
var visor = null;

// Função que será chamada sempre que clicar em um botão
function cliqueBotao(e) {
	// Mostrar o MouseEvent no console, para melhor entendimento
	console.log(e);
	// Usamos a propriedade toElement do MouseEvent para pegar o elemento que foi clicado (no caso o input)
	var input = e.toElement;
	// Verificamos se o input não é nulo para evitar problemas
	if (input) {
		// Usamos o atributo data-role definido no HTML em um switch
		switch (input.getAttribute('data-role')) {
			// data-role="numero"
			case 'numero':
				// Se o usuário pressionou um operador anteriormente, temos que limpar o visor antes de colocar o número
				if (pressionouOperador) {
					visor.value = 0;
					pressionouOperador = false;
				}
				// Concatenamos o valor no visor com o valor do botão clicado
				visor.value = parseInt(visor.value + input.value);
				break;
			// data-role="operador"
			case 'operador':
				// Armazenamos o que estava no visor na variável cache1
				cache1 = parseInt(visor.value);
				// Armazenamos o operador clicado na variável cacheOperador
				cacheOperador = input.value;
				pressionouOperador = true;
				break;
			// data-role="igual"
			case 'igual':
				// Verificação básica se as variáveis cache1 e cacheOperador não estão vazias
				if (cache1 != null && cacheOperador != null) {
					// Exibir no console o que será passado para o eval
					console.log(cache1 + ' ' + cacheOperador + ' ' + visor.value);
					// Usamos a função eval para realizar a operação, e setar seu resultado no visor
					visor.value = eval(cache1 + ' ' + cacheOperador + ' ' + visor.value);
					// Limpamos os caches
					cache1 = null;
					cacheOperador = null;
				}
				break;
			// data-role="limpar"
			case 'limpar':
				visor.value = 0;
				cache1 = null;
				cacheOperador = null;
				break;
			// Algum outro input, se for o caso sair da função
			default:
				return;
		}
	}
}

// Pra quem entende de jQuery, o método abaixo é o equivalente de: $('input').on('click', function(e) { cliqueBotao(e); });
// Adiciona um eventListener que vai aguardar até que todo o conteúdo do DOM seja carregado, ou seja, a página termine de carregar
document.addEventListener('DOMContentLoaded', function() {
	// Usamos a função getElementById para pegar o element do visor 
	visor = document.getElementById('visor');
	// Usamos a função getElementsByTagName para pegar todos os inputs
	var inputs = document.getElementsByTagName('input');
	// Percorrer entre todos os inputs
	for (var i = 0; i < inputs.length; i++) {
		var input = inputs[i];
		// Adicionar um listener para clique em cada um dos inputs, que quando acionado chamará a função cliqueBotao
		// passando como parâmetro o MouseEvent do clique (e)
		input.addEventListener('click', function(e) { cliqueBotao(e); });
	}
}, false);

